import React, { Component } from "react";
import "./App.css";

import Header from "../Header/Header";
import SearchBar from "../SearchBar/SearchBar";
import ResultsContainer from "../ResultsContainer/ResultsContainer";

const getSuggestedNames = require("@rstacruz/startup-name-generator");
class App extends Component {
  state = {
    headerText: "Name it!",
    headerExpanded: true,
    suggestedNames: [],
  };

  handleInputChange = (inputText) => {
    this.setState({ suggestedNames: getSuggestedNames(inputText) });
    if (inputText !== "") {
      this.setState({ headerExpanded: false });
    } else {
      this.setState({ headerExpanded: true, suggestedNames: [] });
    }
  };

  render() {
    return (
      <div>
        <Header
          headerText={this.state.headerText}
          headerExpanded={this.state.headerExpanded}
        ></Header>
        <SearchBar onInputChange={this.handleInputChange}></SearchBar>
        <ResultsContainer
          suggestedNames={this.state.suggestedNames}
        ></ResultsContainer>
      </div>
    );
  }
}

export default App;
