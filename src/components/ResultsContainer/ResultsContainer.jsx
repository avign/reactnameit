import React, { Component } from "react";
import "./ResultsContainer.css";
import NameCard from "../NameCard/NameCard";

const ResultsContainer = ({ suggestedNames }) => {
  let suggestedNamesJsx = suggestedNames.map((suggestedName) => {
    return (
      <NameCard key={suggestedName} suggestedName={suggestedName}></NameCard>
    );
  });
  if (suggestedNames.length === 0) suggestedNamesJsx = null;
  console.log(suggestedNamesJsx);
  return <div className="results-container">{suggestedNamesJsx}</div>;
};

export default ResultsContainer;
