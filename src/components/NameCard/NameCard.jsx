import React, { Component } from "react";
import "./NameCard.css";
const NameCard = ({ suggestedName }) => {
  return (
    <div className="name-card-container">
      <p className="name-card-text">{suggestedName}</p>
    </div>
  );
};

export default NameCard;
