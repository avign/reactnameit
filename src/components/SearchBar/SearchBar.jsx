import React, { Component } from "react";
import "./SearchBar.css";
const SearchBar = ({ onInputChange }) => {
  return (
    <div className="SearchBar">
      <input
        className="SearchBar-Text"
        onChange={(event) => onInputChange(event.target.value)}
        type="text"
        placeholder="Type keywords"
      ></input>
    </div>
  );
};

export default SearchBar;
